import React from 'react';
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import visualApp from 'constants/reducers'
import Button from 'components/common/button';
import CodeMirrorContainer from 'components/codemirror';
import ClipboardContainer from 'components/clipboard';
import DiscordView from 'components/discordview/discordview';

let store = createStore(visualApp)

const FooterButton = (props) => {
  return <Button {...props} className='shadow-1 shadow-hover-2 shadow-up-hover' />;
};

const App = React.createClass({
  // TODO: serialize input, webhookMode, compactMode and darkTheme to query string?

  getInitialState() {
    return {
      webhookMode: false,
      compactMode: false,
      darkTheme: true,
      error: null,
    };
  },

  componentWillMount() {
    //this.validateInput(this.state.input, this.state.webhookMode);
  },

  onCodeChange(value, change) {
    // for some reason this fires without the value changing...?
    /*if (value !== this.state.input) {
      this.validateInput(value, this.state.webhookMode);
    }*/
  },

  toggleWebhookMode() {
    //this.validateInput(this.state.input, !this.state.webhookMode);
  },

  toggleTheme() {
    this.setState({ darkTheme: !this.state.darkTheme });
  },

  toggleCompactMode() {
    this.setState({ compactMode: !this.state.compactMode });
  },

  updateError(err) {
    this.setState({ error: err })
  },

  render() {
    const webhookModeLabel = `${this.state.webhookMode ? 'Dis' : 'En'}able webhook mode`;
    const themeLabel = `${this.state.darkTheme ? 'Light' : 'Dark'} theme`;
    const compactModeLabel = `${this.state.compactMode ? 'Cozy' : 'Compact'} mode`;

    return (
      <Provider store={store}>
        <main className="px-2 overflow-hidden">

          <div className="row">
              <div className="col-lg-6 col-sm-12 py-sm-4 py-lg-0 d-flex flex-column rounded-3">
                <DiscordView
                  error={this.state.error}
                  webhookMode={this.state.webhookMode}
                  darkTheme={this.state.darkTheme}
                  compactMode={this.state.compactMode}
                />
              </div>
              <div className="col-lg-6 col-sm-12 py-sm-4 py-lg-0 rounded-3">
                <div>
                  <ClipboardContainer />
                </div>
                <div className="d-flex h-100">
                  <CodeMirrorContainer
                    theme={'one-dark'}
                    updateError={this.updateError}
                  />
                </div>
              </div>
            <footer className="w-100 pa3 tc white">
            </footer>
          </div>
        </main>
      </Provider>
    );
  },
});


export default App;
