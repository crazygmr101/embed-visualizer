import React from 'react';
import { SketchPicker  } from 'react-color';

class EmbedColorPill extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      isEdited: false,
      previous: null
    }
  }

  handleChangeComplete = (color) => {
    this.setState({isEdited: false, previous: null});
  };

  handleChange = (color) => {
    this.props.onUpdate(color.hex)
  }

  handleCancel = (color) => {
    this.props.onUpdate(this.state.previous);
    this.setState({isEdited: false, previous: null})
  }

/*    if (color) {
      const r = (color >> 16) & 0xFF;
      const g = (color >> 8) & 0xFF;
      const b = color & 0xFF;
      computed = `rgba(${r},${g},${b},1)`;
    }
*/
  render(){
    const style = { backgroundColor: this.props.color };
    return this.state.isEdited ?
        <span className="px-1">
          <div className="btn-group">
            <div className="btn btn-outline-success d-block my-2" onClick={this.handleChangeComplete}>Save</div>
            <div className="btn btn-outline-danger d-block my-2" onClick={this.handleCancel}>Cancel</div>
          </div>
            <SketchPicker
                color={this.props.color}
                onChangeComplete={this.handleChange}
                width={400}
                className="py-1"
                disableAlpha={true}
                presetColors={["#921616", "#a72323", "#a91409", "#7f1818",
                  "#510008", "#6d0b3f", "#9b1349", "#b51249",
                  "#8d0d38", "#570924", "#3b1070", "#621982",
                  "#7d1f8d", "#742f80", "#451c4c", "#271675",
                  "#412486", "#522e92", "#472c78", "#2f3335",
                  "#151c65", "#26327f", "#324191", "#2d3975",
                  "#2f3234", "#0a3981", "#145ea8", "#0a6ab6",
                  "#08528d", "#2c3032", "#01467c", "#026da7",
                  "#0287c3", "#076d9a", "#04425f", "#004d50",
                  "#007986", "#0096aa", "#198896", "#0f525a",
                  "#003e33", "#006156", "#00786d", "#3b8f89",
                  "#22524f", "#143e29", "#2d7230", "#3d8c40",
                  "#316f43", "#1f4429", "#295418", "#537f2d",
                  "#719331", "#577226", "#374717", "#685f12",
                  "#8c9022", "#99a21d", "#777e16", "#4b4f0f",
                  "#bc5c08", "#b07f03", "#a99700", "#857800",
                  "#564e00", "#cc5900", "#cc8000", "#c89600",
                  "#9d7700", "#614800", "#b84100", "#c46300",
                  "#cc7a00", "#9e5e00", "#613a00", "#992b0a",
                  "#b83b14", "#b82c00", "#8f2200", "#5b1600",
                  "#321f1c", "#4a332c", "#61443a", "#6a554e",
                  "#3f322d", "#1e282d", "#374850", "#4d646f",
                  "#465760", "#2b353a", "#000000", "#3e4345",
                  "#535a5e", "#2d3133", "#181a1b", "#c8c3bc",
                  "#E0BBE4", "#957DAD", "#D291BC", "#FEC8D8",
                  "#FFDFD3", "#d5d6ea", "#f6f6eb", "#d7ecd9",
                  "#f5d5cb", "#f6ecf5", "#f3ddf2"
                ]}
            />
        </span>
  :
    <div
        className="embed-color-pill"
        style={style}
        onClick={() => this.setState({isEdited: true, previous: this.props.color})}/>;
  }
}
export default EmbedColorPill
